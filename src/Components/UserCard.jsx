import React, { useEffect, useState } from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { Form, Button, Col, Row } from 'react-bootstrap';
import { toast, ToastContainer } from 'react-toastify';
import { Link, useNavigate } from "react-router-dom";
import { useDispatch } from 'react-redux';
import { updateUser, deleteUser } from '../Redux/Actions/userAction';
import { CardMedia } from '@mui/material';


const BikeItem = ({ user }) => {
  const [editMode, setEditMode] = useState(false);
  const [email, setEmail] = useState("");
  const [role, setRole] = useState("")

  const navigate = useNavigate()
  const dispatch = useDispatch()

  const updateHandler = (e) => {
    e.preventDefault();
    if (email.length === 0) {
      toast("Email Cannot be empty")
      return;
    }
    dispatch(updateUser({ id: user.id, role: role, email: email }))
    toast(`${email.slice(0, email.indexOf('@'))} details updated`)
  }

  const deleteHandler = (id) => {
    dispatch(deleteUser(id));
    navigate('/manage/users');
  }

  useEffect(() => {
    if (user) {
      setEmail(user.email)
      setRole(user.role)
    }
  }, [user, dispatch])

  return (
      <Card style={{ boxShadow: "2px 5px 8px rgba(1,1,1,1)", width:"30vw" }} className="mx-4 my-5 p-3">
        <ToastContainer />
        <Row>
        {/* {editMode ?
                <Form.Group controlId='email' className='my-3'>
                <Form.Label>Email Address :</Form.Label>
                <Form.Control
                  type='email'
                  placeholder='Enter Email'
                  value={email}
                  onChange={(e)=>setEmail(e.target.value)}
                ></Form.Control>
              </Form.Group> : */}
                <Typography gutterBottom variant="h5" className="mx-2 d-flex justify-content-center" component="div">{email}</Typography>
              
        <Col className='col-md-5'>
            <Link to={`/bikes/${user.id}`}>
              <CardMedia
                component="img"
                alt={user.role}
                style={{width:"100%" }}
                image={`/images/${user.role}.png`}
              />
            </Link>
          </Col>
          <Col className='col-md-7'>
          <CardContent>
            <Form onSubmit={updateHandler}>
              {/* form container starts */}
              

              {editMode ?
                <Form.Group controlId='role' className='my-3'>
                  <Form.Label>Role :</Form.Label>
                  <Form.Control
                    as='select'
                    value={role}
                    onChange={(e) => setRole(e.target.value)}
                  >
                    <option value="USER">USER</option>
                    <option value="MANAGER">MANAGER</option>
                  </Form.Control>
                </Form.Group> :
                <Typography gutterBottom variant="h5" component="div">
                    <Button variant="outline-dark" disabled>{role}</Button>
                </Typography>
              }

              {editMode &&
               <> <Button
                  type="submit"
                  variant='outline-success'
                >Update</Button>
                <Button variant="outline-warning" onClick={() => setEditMode(false)}> Cancel </Button></>}

              {/* form container ends */}
            </Form>
           {!editMode? <> <Button variant="outline-warning" onClick={() => setEditMode(prev => !prev)}> Edit &nbsp;&nbsp;</Button>
              <Button variant="outline-danger" onClick={() => deleteHandler(user.id)}> Delete </Button> </> : null}
          </CardContent>
          </Col>
        </Row>
      </Card>
  );
};

export default BikeItem;
