import { Container } from '@mui/system'
import React from 'react'
import { Spinner } from "react-bootstrap"
import PulseLoader from "react-spinners/PulseLoader"
const Loader = () => {
  return (
    <Container fluid className="d-flex justify-content-center py-3">
    <PulseLoader
        color='black'
        
        loading={true}
        size={15}
        margin={2}
    >
        <span className='sr-only'>Loading...</span>
    </PulseLoader>
    </Container>
  )
}

export default Loader