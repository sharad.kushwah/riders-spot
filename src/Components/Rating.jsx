import React from 'react'

const Rating = ({ value,color }) => {
  return (
    <div className='rating'>
        <span>
            <i style={{color}} className={value>=1?'fas fa-star':"far fa-star" }></i>
        </span>
        <span>
            <i style={{color}} className={value>=2?'fas fa-star':"far fa-star" }></i>
        </span>
        <span>
            <i style={{color}} className={value>=3?'fas fa-star':"far fa-star" }></i>
        </span>
        <span>
            <i style={{color}} className={value>=4?'fas fa-star':"far fa-star" }></i>
        </span>
        <span>
            <i style={{color}} className={value>=5?'fas fa-star':"far fa-star" }></i>
        </span>
    </div>
  )
}

Rating.defaultProps ={
    color : "orange"
}

export default Rating