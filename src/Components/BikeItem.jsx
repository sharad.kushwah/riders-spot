import React, { useEffect, useState } from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import DatePicker from "react-datepicker";
import Rating from "./Rating";
import { Form, Button, Col, Container, Row, Popover, OverlayTrigger } from 'react-bootstrap';
import { toast, ToastContainer } from 'react-toastify';
import { Link, useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import { bikeList, updateBike } from '../Redux/Actions/bikeAction';
import { bikeListFailReset, bikeUpdateReset } from '../Redux/Constants/bikeConstants';
import { deleteBike } from '../Redux/Actions/bikeAction';

const BikeItem = ({ bike }) => {
  const [editMode, setEditMode] = useState(false);
  const [model, setModel] = useState(bike.model);
  const [color, setColor] = useState(bike.color);
  const [location, setLocation] = useState(bike.location);
  const [avgRating, setAvgRating] = useState(bike.avgRating);
  const [startDate, setStartDate] = useState(bike.startDate);
  const [endDate, setEndDate] = useState(bike.endDate);
  const [isAvailable, setIsAvailable] = useState(bike.isAvailable);

  const navigate = useNavigate()
  const dispatch = useDispatch()

  const { loggedInUser } = useSelector(state => state.userLogin)
  const { success: successUpdate } = useSelector(state => state.bikeUpdate)

  function convertDateFromMStoDate(dateInMs) {
    dateInMs = new Date(dateInMs);
    let date = dateInMs.getDate();
    let month = dateInMs.getMonth() + 1;
    let year = dateInMs.getFullYear();
    return `${date}/${month}/${year}`;
  }

  const deleteHandler = (id) => {
    dispatch(deleteBike(id));
    navigate('/bikes/page/1')
  }

  const updateHandler = (e) => {

    e.preventDefault()
    if (model.length === 0) {
      toast("Model cannot be empty")
      return;
    }

    if (color.length === 0) {
      toast("Color cannot be empty")
      return
    }

    if (location.length === 0) {
      toast("location cannot be empty")
      return;
    }

    if (avgRating.length === 0) {
      toast("Average rating cannot be empty")
      return
    }

    if (avgRating < 0 || avgRating > 5) {
      toast("Enter Valid Rating")
      return
    }
    if (startDate === "" || endDate === "") {
      toast("Dates cannot be empty")
      return;

    }
    console.log(bike.id, model, color, location, avgRating, startDate, endDate, isAvailable)
    dispatch(updateBike({
      id: bike.id,
      model,
      color,
      location,
      avgRating,
      startDate,
      endDate,
      isAvailable: isAvailable === 'AVAILABLE' ? true : isAvailable === 'UNAVAILABLE' ? false : true
    }))

    setEditMode(false);
  }

  useEffect(() => {
    if (successUpdate) {
      dispatch({ type: bikeUpdateReset })
    }
  }, [dispatch, navigate, successUpdate])

  const popover = (
    <Popover id="popover-basic">
      <Popover.Header as="h3">AVAILABLE!!!</Popover.Header>
      <Popover.Body>
        Available between <strong>{convertDateFromMStoDate(bike.startDate)}</strong> and <strong>{convertDateFromMStoDate(bike.endDate)}</strong>. It's very engaging.
        right?
      </Popover.Body>
    </Popover>
  );

  const Example = () => (
    <OverlayTrigger trigger="click" placement="right" overlay={popover}>
      <Button variant="outline-success">Available</Button>
    </OverlayTrigger>
  );

  const popover1 = (
    <Popover id="popover1-basic">
      <Popover.Header as="h3">Oops.. Unavailable</Popover.Header>
      <Popover.Body>
        Not available right now. Will be sooon!!
      </Popover.Body>
    </Popover>
  );

  const Example1 = () => (
    <OverlayTrigger trigger="click" placement="right" overlay={popover1}>
      <Button variant="outline-danger">Unavailable</Button>
    </OverlayTrigger>
  );


  return (
    <>
      <Card style={{ boxShadow: "2px 5px 8px rgba(1,1,1,1)" }} className="my-5 p-3">
        <ToastContainer />
        <Row>
          <Col>
            <Link to={`/bikes/${bike.id}`}>
              <CardMedia
                component="img"
                alt={bike.model}
                height="310"
                image={`/images/${bike.image}`}
              />
            </Link>
          </Col>
          <Col>
            <CardContent>

              <Form onSubmit={updateHandler}>
                {/* form container starts */}


                {editMode ?
                  <Form.Group as={Row} controlId='model' className='my-2'>
                    <Form.Label className="my-1">Model:</Form.Label>
                    <Form.Control
                      type='text'
                      value={model}
                      onChange={(e) => setModel(e.target.value)}
                      className="my-1"
                    ></Form.Control>
                    <Form.Label className="my-1">Location:</Form.Label>
                    <Form.Control
                      type='text'
                      value={location}
                      onChange={(e) => setLocation(e.target.value)}
                      className="my-1"
                    ></Form.Control>
                  </Form.Group> :
                  <Typography gutterBottom variant="h5" component="div">
                    <Link to={`/bikes/${bike.id}`} className="text-reset">{bike.model}&nbsp;in&nbsp;{bike.location}&nbsp;&nbsp;</Link>
                  </Typography>
                }
                <Typography gutterBottom variant="h5" component="div">
                  <Rating value={bike.avgRating} />
                </Typography>
                {editMode ?
                  <Form.Group controlId='color' className='my-3'>
                    <Form.Label>Color :</Form.Label>
                    <Form.Control
                      type='text'
                      value={color}
                      onChange={(e) => setColor(e.target.value)}
                    ></Form.Control>
                  </Form.Group> :
                  <Typography gutterBottom variant="h5" component="div">{bike.color}</Typography>
                }

                {editMode ?
                  <Form.Group>
                    <Form.Label>Status :</Form.Label>
                    <Form.Control
                      as='select'
                      value={isAvailable}
                      onChange={(e) => setIsAvailable(e.target.value)}
                    >
                      <option value="AVAILABLE">AVAILABLE</option>
                      <option value="UNAVAILABLE">UNAVAILABLE</option>
                    </Form.Control>
                  </Form.Group> :

                  <Typography gutterBottom variant="h5" component="div">
                    {bike.isAvailable ? <Example /> : <Example1 />}
                  </Typography>
                }

                {editMode ?
                  <Row>
                    <Col>
                      <Form.Group controlId="activeDate" className="my-2 mx-1 form-inline">
                        <Form.Label className="col-xs-2"> Select Start Date :&nbsp;&nbsp; </Form.Label>
                        <div className="col-xs-10">
                          <DatePicker
                            className="p-2"
                            selected={startDate}
                            onChange={(date) => setStartDate(date.getTime())}
                            minDate={new Date()}
                            dateFormat="dd/MM/yyyy"
                            onKeyDown={(e) => {
                              e.preventDefault();
                            }}

                          /></div>
                      </Form.Group>
                    </Col>

                    <Col>
                      <Form.Group controlId="activeDate" className="my-2 mx-1 form-inline">
                        <Form.Label className="col-xs-2"> Select End date :&nbsp;&nbsp;</Form.Label>
                        <div className="col-xs-10">
                          <DatePicker
                            className="p-2"
                            selected={endDate}
                            onChange={(date) => setEndDate(date.getTime())}
                            minDate={new Date()}
                            dateFormat="dd/MM/yyyy"
                            onKeyDown={(e) => {
                              e.preventDefault();
                            }}
                          /></div>
                      </Form.Group>
                    </Col>
                  </Row> : null
                }
                {editMode &&
                  <><Button
                    className='mb-3'
                    type="submit"
                    variant='outline-success'
                  >Update</Button>
                  <Button variant="outline-danger" className='mb-3' onClick={() => setEditMode(prev => !prev)}> Cancel</Button></>}
                {/* form container ends */}
              </Form>
              {(loggedInUser.user.role === "MANAGER") &&
                <Container className="mb-3 px-0 mx-0">
                  <Link to={`/reservations/${bike.id}`}>
                    {!editMode && <Button variant="outline-warning" > Show Reservations &nbsp;&nbsp;</Button>}
                  </Link>
                  {editMode ? null : <Button variant="outline-warning" onClick={() => setEditMode(prev => !prev)}> Edit &nbsp;&nbsp;</Button>}
                  {!editMode && <Button variant="outline-danger" onClick={() => deleteHandler(bike.id)}> Delete </Button>}
                </Container>}
            </CardContent>
            {(loggedInUser.user.role === "USER") && <CardActions>
              <a href="https://www.linkedin.com/in/sharad-kushwah/"><Button variant="dark" className='mx-3' >Learn More</Button></a>
            </CardActions>}
          </Col>
        </Row>
      </Card>
    </>
  );
};

export default BikeItem;
