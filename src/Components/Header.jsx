import React from 'react'
import { Container,Navbar,Nav,NavDropdown } from "react-bootstrap";
import { LinkContainer } from 'react-router-bootstrap';
import { useDispatch,useSelector } from 'react-redux';
import { logout } from "../Redux/Actions/userAction"
const Header = () => {
  const dispatch=useDispatch()
  const userLogin=useSelector(state=>state.userLogin)
  const { loggedInUser }=userLogin

  const logoutHandler = () => {
    dispatch(logout())
  }  

  return (
    <header>
      <Navbar className="h-9"bg="dark" variant='dark' expand="lg" collapseOnSelect>
          <LinkContainer to="/" className=''>
            <Navbar.Brand><b>RIDERS SPOT</b></Navbar.Brand>
          </LinkContainer>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">
              {loggedInUser &&
                loggedInUser.user.role==="MANAGER"?(
                  <NavDropdown title='Manage'>
                    <LinkContainer to={`/manage/users`}>
                      <NavDropdown.Item>
                        users
                      </NavDropdown.Item>
                    </LinkContainer>
                    <LinkContainer to={`/manage/reservations/users`}>
                      <NavDropdown.Item>
                        Reservation Users
                      </NavDropdown.Item>
                    </LinkContainer>
                    <LinkContainer to={`/manage/reservations/bikes`}>
                      <NavDropdown.Item>
                        Reservation Bikes
                      </NavDropdown.Item>
                    </LinkContainer>
                    <LinkContainer to={`/manage/${loggedInUser.user.id}/myreservations`}>
                      <NavDropdown.Item>
                        My Reservations
                      </NavDropdown.Item>
                    </LinkContainer>
                  </NavDropdown>
                ):""
              }
              {loggedInUser?(
                <NavDropdown title={loggedInUser.user.email}>
                  {
                    loggedInUser &&
                    loggedInUser.user.role==="USER" && (
                      <LinkContainer to={`/users/${loggedInUser.user.id}/reservations`}>
                        <NavDropdown.Item>
                          reservations
                        </NavDropdown.Item>
                      </LinkContainer>
                    )
                  }
                    <NavDropdown.Item onClick={logoutHandler}>
                      logout
                    </NavDropdown.Item>
                </NavDropdown>
              ):(
                <LinkContainer to="/login">
                  <Nav.Link>login/sign up</Nav.Link>
                </LinkContainer>
              )}
            </Nav>
          </Navbar.Collapse>
      </Navbar>
    </header>
  )
}

export default Header