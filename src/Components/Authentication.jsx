import React from 'react'
import { Navigate } from "react-router-dom"
import { useSelector } from "react-redux"
const Authentication = ({ children,accessRole }) => {
    const userLogin=useSelector(state=>state.userLogin)
    const { loggedInUser }=userLogin

    if(!loggedInUser){
        return <Navigate to="/login"/>
    }
    if(!accessRole)
      return children

    if(accessRole && loggedInUser && accessRole===loggedInUser.user.role){
      return children;
    }else{
      return <Navigate to="/login"/>
    }
}

export default Authentication