export const bikeListRequest = 'bikeListRequest';
export const bikeListSuccess = 'bikeListSuccess';
export const bikeListBikeUpdate = 'bikeListBikeUpdate';
export const bikeListFail = 'bikeListFail';
export const bikeListFailReset = 'bikeListFailReset';

export const bikeDetailsRequest = 'bikeDetailsRequest';
export const bikeDetailsSuccess = 'bikeDetailsSuccess';
export const bikeDetailsFail = 'bikeDetailsFail';
export const bikeDetailsFailReset = 'bikeDetailsFailReset';

export const bikeCreateRequest = 'bikeCreateRequest';
export const bikeCreateSuccess = 'bikeCreateSuccess';
export const bikeCreateFail = 'bikeListFail';
export const bikeCreateFailReset = 'bikeListFailReset';

export const bikeUpdateRequest = 'bikeUpdateRequest';
export const bikeUpdateSuccess = 'bikeUpdateSuccess';
export const bikeUpdateFail = 'bikeUpdateFail';
export const bikeUpdateReset   = 'bikeUpdateReset';

export const bikeDeleteRequest = 'bikeDeleteRequest';
export const bikeDeleteSuccess = 'bikeDeleteSuccess';
export const bikeDeleteFail = 'bikeDeleteFail';
export const bikeDeleteFailReset = 'bikeDeleteFailReset';

export const managerBikeListRequest = 'managerBikeListRequest';
export const managerBikeListSuccess = 'managerBikeListSuccess';
export const managerBikeListFail = 'managerBikeListFail';
export const managerBikeListFailReset = 'managerBikeListFailReset';