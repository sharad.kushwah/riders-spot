export const resetNotify = (dispatch, actionType, time=3000) => {
    setTimeout(() => {
        dispatch({ type: actionType });
        }, time);
    }