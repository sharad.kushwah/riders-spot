import thunk from "redux-thunk";
import { createStore,combineReducers,applyMiddleware} from "redux";
import { composeWithDevTools } from "redux-devtools-extension";


import { 
    newBikeReducer,
    bikeListReducer,
    bikeDetailsReducer,
    adminBikeListReducer,
    deleteBikeReducer,
    bikeUpdateReducer
} from "./Reducers/bikeReducer"

import {
    reviewCreateReducer,
    reviewListReducer,
} from "./Reducers/reviewReducer"

import {
    userLoginReducer,
    userRegisterReducer,
    userReservationsListReducer,
    usersListReducer,
    userDetailsReducer,
    userDetailsUpdateReducer,
    userDeleteReducer,
    userCreateReducer
} from "./Reducers/userReducer"

import {
    reservationListReducer,
    reservationCreatorReducer,
    reservationCancelReducer,
    reservationUsersListReducer,
    reservationBikesListReducer,
} from "./Reducers/reservationReducer"

const storedUserInformation=localStorage.getItem('loggedInUser') ? JSON.parse(localStorage.getItem('loggedInUser')):null;
const initState={
    userLogin:{
        loggedInUser: storedUserInformation
    }
}

const reducer = combineReducers({
    bikeList: bikeListReducer,
    bikeDetails: bikeDetailsReducer,
    newBike: newBikeReducer,
    bikeUpdate:bikeUpdateReducer,
    deleteBike: deleteBikeReducer,
    userLogin: userLoginReducer,
    userRegister: userRegisterReducer,
    userReservationsList: userReservationsListReducer,
    usersList: usersListReducer,
    userCreate: userCreateReducer,
    adminBikeList: adminBikeListReducer,
    userDetails: userDetailsReducer,
    userDetailsUpdate: userDetailsUpdateReducer,
    userDelete: userDeleteReducer,
    reservationList: reservationListReducer,
    reservationCreator: reservationCreatorReducer,
    reservationCancel: reservationCancelReducer,
    reviewList: reviewListReducer,
    reviewCreate: reviewCreateReducer,
    reservationBikesList: reservationBikesListReducer,
    reservationUsersList: reservationUsersListReducer
})

const store=createStore(reducer, initState, composeWithDevTools(applyMiddleware(thunk)))

export default store