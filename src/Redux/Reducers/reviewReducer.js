import * as reviewTypes from "./../Constants/reviewConstants";

export const reviewListReducer=(state={ reviews:[] },action)=>{
    switch(action.type){
        case reviewTypes.reviewListRequest:
            return { loading:true }
        case reviewTypes.reviewListSuccess:
            return { loading: false,reviews:action.payload }
        case reviewTypes.addReviewToList:
            return { loading: false,reviews:[...state.reviews,action.payload]}
        case reviewTypes.reviewListFail:
            return { loading: false,error: action.payload}
        case reviewTypes.reviewListFailReset:
            return { loading: false,error: null}
        default:
            return state
    }
}

export const reviewCreateReducer = (state={review:null},action) => {
    switch(action.type){
        case reviewTypes.reviewCreateRequest:
            return { loading: true}
        case reviewTypes.reviewCreateSuccess:
            return { loading:false,review:action.payload }
        case reviewTypes.reviewCreateFail:
            return { loading:false,error: action.payload}
        case reviewTypes.reviewCreateFailReset:
            return { loading:false,error: null}
        default:
            return state
    }
}