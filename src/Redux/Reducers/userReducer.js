import * as userTypes from "./../Constants/userConstants"

export const userLoginReducer=(state={},action)=>{
    switch(action.type){
        case userTypes.userLoginRequest:
            return {loading:true}
        case userTypes.userLoginSuccess:
            return {loading:false,loggedInUser: action.payload}
        case userTypes.userLoginFail:
            return {loading:false,error: action.payload}
        case userTypes.userLoginFailReset:
            return {loading:false,error: null}
        case userTypes.userLogout:
            return {}
        default:
            return state
    }
}

export const userRegisterReducer=(state={},action)=>{
    switch(action.type){
        case userTypes.userSignupRequest:
            return {loading:true}
        case userTypes.userSignupSuccess:
            return {loading:false,loggedInUser: action.payload}
        case userTypes.userSignupFail:
            return {loading:false,error: action.payload}
        case userTypes.userSignupFailReset:
            return {loading:false,error: null}
        default:
            return state
    }
}

export const userReservationsListReducer=(state={reservation:[]},action)=>{
    switch(action.type){
        case userTypes.userReservationListRequest:
            return { loading: true }
        case userTypes.UserReservationListSuccess:
            return { loading:false, reservations:action.payload }
        case userTypes.userReservationListFail:
            return { loading:false, error: action.payload }
        case userTypes.userReservationListFailReset:
            return { loading:false, error: null }
        default:
            return state;
    }
} 

export const usersListReducer=(state={ users:[] },action)=>{
    switch(action.type){
        case userTypes.userListRequest:
            return { loading: true }
        case userTypes.userListSuccess:
            return { 
                loading: false,
                users: action.payload.data,
                totalCount: action.payload.totalCount
            }
        case userTypes.userListFail:
            return { loading: false,error: action.payload }
        case userTypes.userListFailReset:
            return { loading: false,error: null }
        default:
            return state;
    }
}

export const userDetailsReducer = (state = { user: { } },action) => {
    switch(action.type){
        case userTypes.userDetailsRequest:
            return { loading: true }
        case userTypes.userDetailsSuccess:
            return { loading: false,user: action.payload }
        case userTypes.userDetailsFail:
            return { loading: false,error: action.payload }
        case userTypes.userDetailsFailReset:
            return { loading: false,error: null }
        default:
            return state;
    }
}

export const userDetailsUpdateReducer = (state = { user: { } },action) => {
    switch(action.type){
        case userTypes.userDetailsUpdateRequest:
            return { loading: true }
        case userTypes.userDetailsUpdateSuccess:
            return { loading: false,user: {...state.user,...action.payload} }
        case userTypes.userDetailsUpdateFail:
            return { loading: false,error: action.payload }
        case userTypes.userDetailsUpdateFailReset:
            return { loading: false,error: null }
        default:
            return state;
    }
}

export const userDeleteReducer = (state = { },action) => {
    switch(action.type){
        case userTypes.userDeleteRequest:
            return { loading: true }
        case userTypes.userDeleteSuccess:
            return { loading: false,user: null }
        case userTypes.userDeleteFail:
            return { loading: false,error: action.payload }
        case userTypes.userDeleteFailReset:
            return { loading: false,error: null }
        default:
            return state;
    }
}

export const userCreateReducer = (state = { user:{} },action) => {
    switch(action.type){
        case userTypes.userCreateRequest:
            return { loading: true }
        case userTypes.userCreateSuccess:
            return { loading: false,user: action.payload, success: true}
        case userTypes.userCreateSuccessReset:
            return { loading: false,user: action.payload, success: false}
        case userTypes.userCreateFail:
            return { loading: false,error: action.payload }
        case userTypes.userCreateFailReset:
            return { loading: false,error: null, success: false }
        default:
            return state;
    }
}