import * as reservationTypes from '../Constants/reservationConstants'


export const reservationListReducer = (state = {reservations :[]}, action)=>{
    switch(action.type){
        case reservationTypes.reservationListRequest :
            return {loading : true}
        case reservationTypes.reservationListSuccess:
            return {loading : false, reservations : action.payload}
        case reservationTypes.reservationListFail:
            return {loading : false, error : action.payload}
        case reservationTypes.reservationListReset :
            return {...state, error : null}
        default :
            return state;
    }
}

export const reservationCreatorReducer = (state = {reservation : {}},action)=>{
    switch(action.type){
        case reservationTypes.reservationCreateRequest :
            return {loading : true}
        case reservationTypes.reservationCreateSuccess :
            return {loading : false, reservation :action.payload, success: true}   
        case reservationTypes.reservationCreateSuccessReset :
            return {...state, success:null} 
        case reservationTypes.reservationCreateFail :
            return {loading : false, error : action.payload}
        case reservationTypes.reservationCreateFailReset :
            return {...state, error : null}
        default : 
            return state;
    }
}

export const reservationCancelReducer = (state={}, action)=>{
    switch(action.type){
        case reservationTypes.reservationCancelRequest :
            return {loading : true}
        case reservationTypes.reservationCancelSuccess :
            return {loading : false, status : action.payload}
        case reservationTypes.reservationCancelFail :
            return {loading : false, error : action.payload}
        case reservationTypes.reservationCancelReset :
            return {...state, error : null}
        default :
            return state;
    }
}

export const reservationUsersListReducer = (state = {usersReservations : []}, action)=>{
    switch(action.type){
        case reservationTypes.reservationUserListRequest:
            return { loading:true }
        case reservationTypes.reservationUserListSuccess:
            return { loading:false, usersReservations: action.payload.resList ,totalCount: action.payload.totalCount }
        case reservationTypes.reservationUserListFail:
            return { loading:false, error: action.payload }
        case reservationTypes.reservationUserListReset :
            return {...state, error : null}
        default:
            return state;
    }
}

export const reservationBikesListReducer = (state={ bikesReservations:[]},action)=>{
    switch(action.type){
        case reservationTypes.reservationBikeListRequest:
            return { loading:true }
        case reservationTypes.reservationBikeListSuccess:
            return { loading:false, bikesReservations: action.payload.resList,totalCount: action.payload.totalCount }
        case reservationTypes.reservationBikeListFail:
            return { loading:false, error: action.payload }
        case reservationTypes.reservationBikeListReset:
            return {loading : false, error : null}
        default:
            return state;
    }
}