import * as bikeTypes from "./../Constants/bikeConstants"

export const bikeListReducer = (state={ bikes:[]},action)=>{
    let newList = state.bikes;
    switch(action.type){
        case bikeTypes.bikeListRequest:
            return {loading:true}
        case bikeTypes.bikeListSuccess:
            return {...state, loading:false,bikes: action.payload.data,pages: action.payload.pages}
        case bikeTypes.bikeListBikeUpdate:
             newList = state.bikes.map((item) => {
                if(item.id === action.payload.data.id)
                    return action.payload.data
                else
                    return item
            })
            return {...state,bikes: newList}    
        case bikeTypes.bikeListFail:
            return {loading:false,error:action.payload}
        case bikeTypes.bikeListFailReset:
            return {...state,error:null}
        default:
            return state
    }
}

export const bikeDetailsReducer=(state={ bike:{} },action)=>{
    switch(action.type){
        case bikeTypes.bikeDetailsRequest:
            return {loading:true}
        case bikeTypes.bikeDetailsSuccess:
            return {loading:false,bike: action.payload}
        case bikeTypes.bikeDetailsFail:
            return {loading:false,error:action.payload}
        case bikeTypes.bikeDetailsFailReset:
            return {...state,error:null}
        default:
            return state
    }
}

export const newBikeReducer = (state = { bike: {} },action)=>{
    switch(action.type){
        case bikeTypes.bikeCreateRequest:
            return { loading: true }
        case bikeTypes.bikeCreateSuccess:
            return { loading: false,bike: action.payload }
        case bikeTypes.bikeCreateFail:
            return { loading:false,error: action.payload }
        case bikeTypes.bikeCreateFailReset:
            return { ...state,error: null }
        default:
            return state;
    }
}


export const bikeUpdateReducer=(state={ bike:{}},action)=>{
    switch(action.type){
        case bikeTypes.bikeUpdateRequest:
            return { loading: true }
        case bikeTypes.bikeUpdateSuccess:
            return { loading: false, success:true }
        case bikeTypes.bikeUpdateFail:
            return { loading: false,error: action.payload }
        case bikeTypes.bikeUpdateReset:
            return { ...state, error: null }
        default:
            return state;
    }
}

export const deleteBikeReducer = (state = { bike:{}},action)=>{
    switch(action.type){
        case bikeTypes.bikeDeleteRequest:
            return { loading: true }
        case bikeTypes.bikeDeleteSuccess:
            return { loading: false, success: true}
        case bikeTypes.bikeDeleteFail:
            return { loading:false,error: action.payload }
        case bikeTypes.bikeDeleteFailReset:
            return { ...state, error: null }
        default:
            return state;
    }
}

export const adminBikeListReducer=(state = { bikes:[] },action)=>{
    switch(action.type){
        case bikeTypes.managerBikeListRequest:
            return { loading: true }
        case bikeTypes.managerBikeListSuccess:
            return { loading: false,bikes: action.payload.data,pages:action.payload.pages }
        case bikeTypes.managerBikeListFail:
            return { loading:false,error: action.payload }
        case bikeTypes.managerBikeListFailReset:
            return { ...state, error: null }
        default:
            return state;
    }
}