import axios from "axios"
import * as reservationTypes from "./../Constants/reservationConstants";
import { resetNotify } from "../Utils";

export const getAllReservations = () => async (dispatch,getState) => {
    try {
        dispatch({type: reservationTypes.reservationListRequest});
        const { userLogin:{ loggedInUser } }=getState();
        const config={headers:{Authorization: `Bearer ${loggedInUser.accessToken}`}};
        const { data }=await axios.get("https://ridersspot.herokuapp.com/reservations",config)
        dispatch({
            type: reservationTypes.reservationListSuccess,
            payload: data
        })
    } catch (err) {
        dispatch({ 
            type: reservationTypes.reservationListFail,
            payload: err.response && err.response.data.message ? err.response.data.message : err.message
          });
        resetNotify(dispatch, reservationTypes.reservationListReset);
    }
}

export const createReservation = (reservation) => async (dispatch,getState) => {
    try {
        dispatch({type: reservationTypes.reservationCreateRequest});
        const { userLogin:{ loggedInUser } }=getState();
        const config={headers:{Authorization: `Bearer ${loggedInUser.accessToken}`}};
        const { data } = await axios.post("https://ridersspot.herokuapp.com/reservations",reservation,config);
        dispatch({
            type: reservationTypes.reservationCreateSuccess,
            payload: data
        })
        resetNotify(dispatch, reservationTypes.reservationCreateSuccessReset);
    } catch (err) {
        dispatch({
            type: reservationTypes.reservationCreateFail,
            payload: err.response && err.response.data.message ? err.response.data.message : err.message
        })
        resetNotify(dispatch, reservationTypes.reservationCreateFailReset);
    }
}

export const cancelReservation = (id) => async (dispatch,getState) => {
    try {
        dispatch({type: reservationTypes.reservationCancelRequest})

        const updateData={ status:"CANCELED",  };
        const { userLogin: { loggedInUser } } =getState();
        const config={headers:{Authorization: `Bearer ${loggedInUser.accessToken}`}}
        const { data } = await axios.patch(`https://ridersspot.herokuapp.com/reservations/${id}`,updateData,config)
        
        dispatch({
            type:reservationTypes.reservationCancelSuccess,
            payload:data
        })
    } catch (err) {
        dispatch({
            type: reservationTypes.reservationCancelFail,
            payload: err.response && err.response.data.message ? err.response.data.message : err.message
        })
        resetNotify(dispatch, reservationTypes.reservationCancelReset);
    }
}

export const getAllBikesInReservation = (page=1, limit=5, bikeId="") => async (dispatch,getState) => {
    try{
        dispatch({
            type : reservationTypes.reservationBikeListRequest
        })
        const {
            userLogin: { loggedInUser },
          } = getState();
    
          const config = {
            headers: {
              Authorization: `Bearer ${loggedInUser.accessToken}`,
            },
          };

          let url = ""
          if(bikeId===""){
            url = `https://ridersspot.herokuapp.com/reservations?_page=${page}&_limit=${limit}`;
          }
          else {
            url = `https://ridersspot.herokuapp.com/reservations?_page=${page}&_limit=${limit}&bikeId=${bikeId}`
          }
            const {data,headers} = await axios.get(url)
         
            console.log(data);
          
          const bikesReservation = [];
          const bike_inRes = []
          for(let i=0; i<data.length; i++){
            let currBikeReservation = await axios.get(`https://ridersspot.herokuapp.com/bikes/${data[i].bikeId}`, config);
            bikesReservation.push(data[i])
            bike_inRes.push(currBikeReservation);
          }

          Promise.all(bike_inRes)
          .then(bikes =>{
                // console.log(bikes)
                const resList =[];
                for(let i=0; i<bikesReservation.length; i++){
                    const newBike = {
                    id: bikes[i].data.id,
                    model : bikes[i].data.model,
                    location : bikes[i].data.location,
                    color : bikes[i].data.color,
                    avgRating : bikes[i].data.avgRating,
                    bikeId: bikesReservation[i].bikeId,
                    startDate: bikesReservation[i].start_date,
                    endDate: bikesReservation[i].end_date,
                    status: bikesReservation[i].status
                    }
                    resList.push(newBike)
                }
            
                dispatch({
                    type : reservationTypes.reservationBikeListSuccess,
                    payload : {resList, totalCount : headers["x-total-count"]}
                })
          })
          .catch((error)=>{
              dispatch({
                  type : reservationTypes.reservationBikeListFail,
                  payload : error.response && error.response.data ? error.response.data : error.message
              })
              resetNotify(dispatch, reservationTypes.reservationBikeListReset);
          })  
    } catch(error){
        dispatch({
            type : reservationTypes.reservationBikeListFail,
            payload : error.response && error.response.data ? error.response.data : error.message
        })
        resetNotify(dispatch, reservationTypes.reservationBikeListReset);
    }
}

export const getAllusersInReservation = (page=1, limit=5) => async (dispatch, getState) => {
    try{
        dispatch({
            type : reservationTypes.reservationUserListRequest
        })
        const {
            userLogin: { loggedInUser },
          } = getState();
    
          const config = {
            headers: {
              Authorization: `Bearer ${loggedInUser.accessToken}`,
            },
          };

          const {data, headers} = await axios.get(`https://ridersspot.herokuapp.com/reservations?_page=${page}&_limit=${limit}`);
          const userReservation = [];
          const users_inRes = [];
          for(let i=0; i<data.length; i++){
            let currUserReservation = axios.get(`https://ridersspot.herokuapp.com/users/${data[i].userId}`, config);
            userReservation.push(data[i]);
            users_inRes.push(currUserReservation)
          }
          Promise.all(users_inRes)
          .then(users =>{
                const resList = [];
                for(let i=0; i<userReservation.length; i++) {
                   const newUser = {id: userReservation[i].id,
                    userEmail: users[i].data.email,
                    role: users[i].data.role,
                    bikeId: userReservation[i].bikeId,
                    startDate: userReservation[i].start_date,
                    endDate: userReservation[i].end_date,
                    status: userReservation[i].status
                    }
                  resList.push(newUser);
                }
            
                dispatch({
                    type : reservationTypes.reservationUserListSuccess,
                    payload : {resList, totalCount : headers["x-total-count"]}
                })
          })
          .catch((error)=>{
            console.log(error)
              dispatch({
                  type : reservationTypes.reservationUserListFail,
                  payload : error.response && error.response.data ? error.response.data : error.message
              })
          })  
    } catch(error){
      console.log(error)
        dispatch({
            type : reservationTypes.reservationUserListFail,
            payload : error.response && error.response.data ? error.response.data : error.message
        })
        resetNotify(dispatch, reservationTypes.reservationUserListReset);
    }
}