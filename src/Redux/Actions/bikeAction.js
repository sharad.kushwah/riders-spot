import axios from "axios";
import * as bikeTypes from "../Constants/bikeConstants";
import { resetNotify } from "../Utils";

export const bikeList = (filter) => async (dispatch, getState) => {
  try {
    dispatch({ type: bikeTypes.bikeListRequest });
    for (let key in filter) {
      if (filter[key] === "" || filter[key] === undefined) {
        delete filter[key];
      }
    }
    const params = new URLSearchParams(filter); // query string
    const {
      userLogin: { loggedInUser },
    } = getState();
    let role = loggedInUser.user.role;
    let url;
    if (role === "USER") {
      url = `https://ridersspot.herokuapp.com/bikes?${params.toString()}&isAvailable=true`;
    } else {
      url = `https://ridersspot.herokuapp.com/bikes?${params.toString()}`;
    }

    const res = await axios.get(url);
  
    dispatch({
      type: bikeTypes.bikeListSuccess,
      payload: {
        data: res.data,
        pages: res.headers["x-total-count"],
      },
    });
  } catch (err) {
    dispatch({
      type: bikeTypes.bikeListFail,
      payload:
        err.response && err.response.data ? err.response.data : err.message,
    });
    resetNotify(dispatch, bikeTypes.bikeListFailReset)
  }
};

export const getBikeDetails = (id) => async (dispatch, getState) => {
  try {
    dispatch({ type: bikeTypes.bikeDetailsRequest });

    const {
      userLogin: { loggedInUser },
    } = getState();

    const header = {
      headers: {
        Authorization: `Bearer ${loggedInUser.accessToken}`,
      },
    };

    const { data } = await axios.get(
      `https://ridersspot.herokuapp.com/bikes/${id}`,
      header
    );
    dispatch({ type: bikeTypes.bikeDetailsSuccess, payload: data });
  } catch (err) {
    dispatch({
      type: bikeTypes.bikeDeleteFail,
      payload:
        err.response && err.response.data ? err.response.data : err.message,
    });
    resetNotify(dispatch, bikeTypes.bikeDeleteFailReset);
  }
};

export const getAllBikes =
  (page = 1, limit = 5) =>
  async (dispatch, getState) => {
    try {
      dispatch({ type: bikeTypes.managerBikeListRequest });
      const {
        userLogin: { loggedInUser },
      } = getState();
      const header = {
        headers: {
          Authorization: `Bearer ${loggedInUser.accessToken}`,
        },
      };

      const res = await axios.get(
        `https://ridersspot.herokuapp.com/bikes?_page=${page}&_limit=${limit}`,
        header
      );
      dispatch({
        type: bikeTypes.managerBikeListSuccess,
        payload: { data: res.data, pages: res.headers["x-total-count"] },
      });
    } catch (err) {
      dispatch({
        type: bikeTypes.managerBikeListFail,
        payload:
          err.response && err.response.data.message
            ? err.response.data.message
            : err.message,
      });
      resetNotify(dispatch, bikeTypes.managerBikeListFailReset);
    }
  };

export const createBike = (bike) => async (dispatch, getState) => {
  try {
    dispatch({ type: bikeTypes.bikeCreateRequest });

    const {
      userLogin: { loggedInUser },
    } = getState();
    const config = {
      headers: { Authorization: `Bearer ${loggedInUser.accessToken}` },
    };

    const { data } = await axios.post(
      "https://ridersspot.herokuapp.com/bikes",
      bike,
      config
    );
    dispatch({
      type: bikeTypes.bikeCreateSuccess,
      payload: data,
    });
  } catch (err) {
    dispatch({
      type: bikeTypes.bikeCreateFail,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    });
    resetNotify(dispatch, bikeTypes.bikeCreateFailReset);
  }
};

export const deleteBike = (id) => async (dispatch, getState) => {
  try {
    dispatch({ type: bikeTypes.bikeDeleteRequest });

    const {
      userLogin: { loggedInUser },
    } = getState();

    const config = {
      headers: { Authorization: `Bearer ${loggedInUser.accessToken}`}
    };

    await axios.delete(`https://ridersspot.herokuapp.com/bikes/${id}`, config);
    dispatch({ type: bikeTypes.bikeDeleteSuccess });

  } catch (err) {
    dispatch({
      type: bikeTypes.bikeDeleteFail,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    });
    resetNotify(dispatch, bikeTypes.bikeDeleteFailReset)
  }
};

export const updateBike = (bike) => async (dispatch, getState) => {
  try {
    dispatch({ type: bikeTypes.bikeUpdateRequest });
    const {
      userLogin: { loggedInUser },
    } = getState();
    const config = {
      headers: {
        Authorization: `Bearer ${loggedInUser.accessToken}`,
      },
    };

    const { data } = await axios.patch(
      `https://ridersspot.herokuapp.com/bikes/${bike.id}`,
      bike,
      config
    );

    dispatch({ type: bikeTypes.bikeUpdateSuccess });
    dispatch({
      type: bikeTypes.bikeDetailsSuccess,
      payload: data
    });

    dispatch({type: bikeTypes.bikeListBikeUpdate, payload: {data: data},})
  } catch (err) {
    dispatch({
      type: bikeTypes.bikeUpdateFail,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    });
    resetNotify(dispatch,bikeTypes.bikeUpdateReset);
  }
};
