import axios from "axios";
import * as reviewTypes from "./../Constants/reviewConstants";
import { resetNotify } from "../Utils";

export const createReview = (review) => async (dispatch, getState) => {
  try {
    dispatch({ type: reviewTypes.reviewCreateRequest });
    const {
      userLogin: { loggedInUser },
    } = getState();
    const config = {
      headers: { Authorization: `Bearer ${loggedInUser.accessToken}` },
    };
    const { data: reservations } = await axios.get(
      `https://ridersspot.herokuapp.com/users/${loggedInUser.user.id}/reservations`, config
    );
    if (reservations.length === 0) {
      throw new Error("Reserve a bike to Review");
    } else if (reservations.length !== 0) {
      console.log(reservations);
      const reservationExist = reservations.find(
        (reservation) =>
          +reservation.bikeId === +review.bikeId &&
          reservation.status === "BOOKED"
      );
      if (!reservationExist) {
        throw new Error("Reservation doesn't exist");
      }
    }
    const { data: userReviews } = await axios.get(
      `https://ridersspot.herokuapp.com/users/${loggedInUser.user.id}/reviews`,
      review,
      config
    );
    if (userReviews.length !== 0) {
      const reviewed = userReviews.find(
        (currReview) => +currReview.bikeId === +review.bikeId
      );
      if (reviewed) {
        throw new Error("You have already reviewed this bike");
      }
    }
    const { data } = await axios.post(
      `https://ridersspot.herokuapp.com/bikes/${review.bikeId}/reviews`,
      review,
      config
    );
    dispatch({ type: reviewTypes.reviewCreateSuccess, payload: data });
    dispatch({ type: reviewTypes.addReviewToList, payload: data });
  } catch (err) {
    console.log(err);
    dispatch({
      type: reviewTypes.reviewCreateFail,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    });
    resetNotify(dispatch, reviewTypes.reviewCreateFailReset);
  }
};

export const listReviews = (bikeId) => async (dispatch, getState) => {
  try {
    dispatch({ type: reviewTypes.reviewListRequest });
    const {
      userLogin: { loggedInUser },
    } = getState();
    const config = {
      headers: { Authorization: `Bearer ${loggedInUser.accessToken}` },
    };
    const { data } = await axios.get(
      `https://ridersspot.herokuapp.com/bikes/${bikeId}/reviews`,
      config
    );
    dispatch({
      type: reviewTypes.reviewListSuccess,
      payload: data,
    });
  } catch (err) {
    dispatch({
      type: reviewTypes.reviewListFail,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    });
    resetNotify(dispatch, reviewTypes.reviewListFailReset);
  }
};
