import axios from "axios";
import * as userTypes from "../Constants/userConstants";
import { resetNotify } from "../Utils";

export const login=(email,password) => async (dispatch) => {
    try {
        dispatch({type: userTypes.userLoginRequest});
        const config={headers:{'Content-type':'application/json'}};
        const {data} = await axios.post("https://ridersspot.herokuapp.com/login", {email,password}, config);
        console.log(data);
        dispatch({type: userTypes.userLoginSuccess, payload: data});
        localStorage.setItem('loggedInUser',JSON.stringify(data));
    } catch (err) {
        dispatch({type: userTypes.userLoginFail, payload: err.response && err.response.data.message ? err.response.data.message : err.message});
        resetNotify(dispatch, userTypes.userLoginFailReset);
    }
}

export const logout=()=>{
    localStorage.removeItem('loggedInUser');
    return{
        type: userTypes.userLogout
    };
}

export const register = (email,password,role="USER") => async (dispatch) => {
    try {
        dispatch({type: userTypes.userSignupRequest});
        const config={headers:{'Content-Type':'application/json'}};
        const { data } = await axios.post('https://ridersspot.herokuapp.com/register',{email,password,role},config);
        
        dispatch({type: userTypes.userSignupSuccess,payload:data});
    } catch (err) {
        dispatch({type: userTypes.userSignupFail, payload: err.response && err.response.data.message ? err.response.data.message : err.message});
        resetNotify(dispatch, userTypes.userSignupFailReset);
    }
}

export const getUsersReservations = (id) => async (dispatch,getState) => {
    try {
        dispatch({type:userTypes.userReservationListRequest});
        const {userLogin: { loggedInUser } } = getState();

        const config={headers:{'Authorization': `Bearer ${loggedInUser.accessToken}`}};
        const { data } = await axios.get(`https://ridersspot.herokuapp.com/users/${+id}/reservations`,config);
        console.log(data);
        dispatch({type: userTypes.UserReservationListSuccess,payload: data});
    } catch (err) {
        dispatch({  
            type: userTypes.userReservationListFail,
            payload:err.response && err.response.data.message ? err.response.data.message : err.message
        })
        resetNotify(dispatch, userTypes.userReservationListFailReset);
    }
}

export const getAllUsers = (page,limit=5) => async (dispatch,getState) => {
    try {
        dispatch({type: userTypes.userListRequest});
        const { userLogin:{ loggedInUser }} = getState();
        const config={headers:{Authorization:`Bearer ${loggedInUser.accessToken}`}};

        const {data, headers} = await axios.get(`https://ridersspot.herokuapp.com/users?_page=${page}&_limit=${limit}`,config)
        dispatch({
            type:userTypes.userListSuccess,
            payload:{
                data: data,
                totalCount: headers["x-total-count"]
            }
        })
    } catch (err) {

        dispatch({type: userTypes.userListFail,payload: err.response && err.response.data.message ? err.response.data.message : err.message});
        resetNotify(dispatch, userTypes.userListFailReset);
    }
}

export const getUserDetails = (id) => async (dispatch,getState) => {
    try {
        dispatch({type: userTypes.userDeleteRequest});
        const { userLogin:{ loggedInUser }}=getState();
        const config={headers:{Authorization:`Bearer ${loggedInUser.accessToken}`}};
        const { data } = await axios.get(`https://ridersspot.herokuapp.com/users/${id}`,config)
        dispatch({type: userTypes.userDetailsSuccess,payload: data});
    } catch (err) {
        dispatch({type: userTypes.userDeleteFail,payload: err.response && err.response.data.message ? err.response.data.message : err.message})
        resetNotify(dispatch, userTypes.userDetailsFailReset);
    }
}

export const updateUser = (user) => async (dispatch,getState) => {
    try {
        dispatch({type: userTypes.userDetailsUpdateRequest});
        const { userLogin:{ loggedInUser }} = getState();
        const config={headers:{Authorization:`Bearer ${loggedInUser.accessToken}`}};
        console.log(user)
        const { data } = await axios.patch(`https://ridersspot.herokuapp.com/users/${user.id}`,user,config);
        dispatch({
            type: userTypes.userDetailsUpdateSuccess,
            payload: data
        })
    } catch (err) {
        dispatch({
            type: userTypes.userDetailsUpdateFail,
            payload: err.response && err.response.data.message ? err.response.data.message : err.message
        })
        resetNotify(dispatch, userTypes.userDetailsUpdateFailReset)
    }
}

export const deleteUser = (id) => async (dispatch,getState) => {
    try {
        dispatch({type: userTypes.userDeleteRequest});
        const { userLogin:{ loggedInUser }} = getState();
        const config={headers:{Authorization:`Bearer ${loggedInUser.accessToken}`}};
        const { data } = await axios.delete(`https://ridersspot.herokuapp.com/users/${id}`,config)
        dispatch({
            type: userTypes.userDeleteSuccess,
            payload: data
        });
    } catch (err) {
        dispatch({type: userTypes.userDeleteFail,payload: err.response && err.response.data.message ? err.response.data.message : err.message});
        resetNotify(dispatch, userTypes.userDeleteFailReset);
    }
}

export const createUser = (user) => async (dispatch,getState) => {
    dispatch({type:userTypes.userCreateRequest})
    try {
        const { userLogin:{ loggedInUser }}=getState()
        const config={
            headers:{
                'Content-Type':'application/json',
                Authorization : `Bearer ${loggedInUser.accessToken}`
            }
        }
    
        const {data} = await axios.post('https://ridersspot.herokuapp.com/users',user,config);
        dispatch({
            type: userTypes.userCreateSuccess,
            payload: data
        })
        resetNotify(dispatch, userTypes.userCreateSuccessReset);
    } catch (err) {
        dispatch({
            type: userTypes.userCreateFail,
            payload: err.response && err.response.data.message ? err.response.data.message : err.message
        })
        resetNotify(dispatch, userTypes.userCreateFailReset);

    }
}