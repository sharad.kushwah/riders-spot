import React,{ useEffect, useState} from 'react';
import { useDispatch, useSelector } from "react-redux";
import { useNavigate,Link } from "react-router-dom";
import { Form,Button } from "react-bootstrap";
import { ToastContainer,toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import FormContainer from '../Components/FormContainer';
import { BsFillArrowLeftCircleFill } from "react-icons/bs"; 
import { createUser } from '../Redux/Actions/userAction';
import ErrorMessage from '../Components/ErrorMessage';

const AddUser = () => {
  const [email,setEmail]=useState("")
  const [password,setPassword]=useState("")
  const [role,setRole]=useState("USER")
  const dispatch=useDispatch()
  const navigate=useNavigate()
  const {users} = useSelector(state=>state.usersList)
  const {error, success} = useSelector(state=>state.userCreate)
  console.log(success)
  const submitHandler=(e)=>{
    e.preventDefault();
    if(email.length===0){
      toast("Email cannot be empty")
      return
    }

   else if(password.length===0){
      toast("Password cannot be empty")
      return
    }
    else{
        dispatch(createUser({email,password,role}))
      }
  }

  useEffect(()=>{
    if(success)
    {
      toast("User created successfully")
      setTimeout(()=>{
        navigate(-1)
      },1000)
    }
  }, [success, navigate])

  return (
  <>
  <Link to= "/bikes" className='m-5'><BsFillArrowLeftCircleFill color="black"size={40} className="my-3"/></Link>
  <FormContainer>
        {error && <ErrorMessage variant="danger">Email Already Exists</ErrorMessage>}
        <ToastContainer />
      <Form>
        <Form.Group controlId='email' className='my-3'>
          <Form.Label>Email Address :</Form.Label>
          <Form.Control
            type='email'
            placeholder='Enter Email'
            value={email}
            onChange={(e)=>setEmail(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Form.Group controlId='password' className='my-3'>
          <Form.Label>Password :</Form.Label>
          <ToastContainer></ToastContainer>
          <Form.Control
            type='password'
            placeholder='Enter password'
            value={password}
            onChange={(e)=>setPassword(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Form.Group controlId='rating'>
            <Form.Label>Role</Form.Label>
            <Form.Control
              as='select'
              value={role}
              onChange={(e)=>setRole(e.target.value)}
              >
              <option value="USER">USER</option>
              <option value="MANAGER">MANAGER</option>
            </Form.Control>
          </Form.Group>
        <Button 
            className='my-3' 
            type="submit" 
            variant='dark'
            onClick={submitHandler}
        >CREATE</Button>
      </Form>
    </FormContainer>
  </>
  )
}

export default AddUser
