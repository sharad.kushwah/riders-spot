import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import Loader from "../Components/Loader"
import ErrorMessage from "../Components/ErrorMessage";
import NoDataFound from "./NoDataFound";
import { Table, Row, Col, Button } from "react-bootstrap";
import { getUsersReservations } from "../Redux/Actions/userAction";
import { cancelReservation } from "../Redux/Actions/reservationAction";
import { updateBike } from "../Redux/Actions/bikeAction";

function ManagerReservations() {
  const params = useParams();
  const [isAvailable, setIsAvailable] = useState(true);
  const userReservationsList = useSelector(
    (state) => state.userReservationsList
  );
  const { loading, error, reservations } = userReservationsList;

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUsersReservations(+params.userId));
  }, [dispatch, +params.userId]);

  const cancelHandler = (id, bikeId) => {
    dispatch(cancelReservation(id));
    dispatch(updateBike({
      id: bikeId,
      isAvailable
    }))
    window.location.reload(false);  
  };

  return (
    <>
      {loading && <Loader />}
      {error && <ErrorMessage variant="danger">{error}</ErrorMessage>}
      {error || (reservations && reservations.length === 0) ? (
        <NoDataFound />
      ) : (
        <Row>
          <Table striped bordered hover responsive className="table-sm">
            <thead>
              <tr>
                <th>ID</th>
                <th>BikeId</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Status</th>
                <th>Cancel</th>
              </tr>
            </thead>
            <tbody>
              {reservations &&
                reservations.map((item) => (
                  <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>{item.bikeId}</td>
                    <td>{item.start_date}</td>
                    <td>{item.end_date}</td>
                    <td>{item.status}</td>
                    <td>
                      <Button
                        size={40}
                        variant="danger"
                        type="button"
                        onClick={() => cancelHandler(item.id, item.bikeId)}
                        disabled={item.status === "CANCELED"}
                      >
                        Cancel
                      </Button>
                    </td>
                  </tr>
                ))}
            </tbody>
          </Table>
        </Row>
      )}
    </>
  );
}

export default ManagerReservations;
