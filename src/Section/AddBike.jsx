import React, { useState } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, Link } from "react-router-dom";
import { Form, Button, Col, Row, Card } from "react-bootstrap";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { IMAGES } from "../Images"
import { BsFillArrowLeftCircleFill } from "react-icons/bs";
import { createBike } from '../Redux/Actions/bikeAction';
import DatePicker from "react-datepicker";
import { CardMedia, CardContent } from '@mui/material';


function AddBike() {
  const [model, setModel] = useState("")
  const [color, setColor] = useState("")
  const [location, setLocation] = useState("")
  const [isAvailable, setIsAvailable] = useState(false)
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");

  const [image, setImage] = useState("default.webp")
  const { bikes } = useSelector(state => state.bikeList)
  console.log(bikes);
  let avgRating = 0;
  for (let i = 0; i < bikes.length; i++) {
    avgRating += parseInt(bikes[i].avgRating)
  }

  avgRating = avgRating / bikes.length;
  console.log(avgRating)
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const submitHandler = (e) => {
    e.preventDefault()
    if (model.length === 0) {
      toast("Model cannot be empty")
      return;
    }

    if (color.length === 0) {
      toast("Color cannot be empty")
      return;
    }

    if (location.length === 0) {
      toast("Loaction cannot be empty")
      return;
    }

    if (avgRating.length === 0) {
      toast("Average rating cannot be empty")
      return;
    }

    if (startDate === "" || endDate === "") {
      toast("Dates cannot be empty")
      return;

    }
    dispatch(createBike({ model, color, location, avgRating, isAvailable: isAvailable === 'AVAILABLE' ? true : isAvailable === 'UNAVAILABLE'? false :null, startDate, endDate, image }))
    toast("Bike Created Successfully")
  }

  const locations = [
    "Noida",
    "Bengaluru",
    "Chennai",
    "Guwahati",
    "Delhi",
    "Mumbai",
    "Gwalior",
    "Gujarat",
    "Kerala",
    "Jammu"
  ];

  const colors = [
    "Black",
    "Blue",
    "Orange",
    "White",
    "Yellow",
    "Green",
    "Red",
  ];

  return (
    <>
      <Card style={{ boxShadow: "2px 5px 8px rgba(1,1,1,1)" }} className="my-5 p-3">
        <ToastContainer />
        <Row>
          <Col>
            <CardMedia
              component="img"
              height="290"
              image={`/images/${image}`}
            />
            <Form.Group controlId='rating'>
              <Form.Label>Image :</Form.Label>
              <Form.Control
                as='select'
                value={image}
                onChange={(e) => setImage(e.target.value)}
              >
                {IMAGES.map((image, index) => {
                  return <option value={image} key={index}>{image}</option>
                })}
              </Form.Control>
            </Form.Group>
          </Col>

          <Col>
            <CardContent>
              <Form onSubmit={submitHandler}>
                <Form.Group controlId='model' className='my-3'>
                  <Form.Label>Model :</Form.Label>
                  <Form.Control
                    type='text'
                    value={model}
                    onChange={(e) => setModel(e.target.value)}
                  ></Form.Control>
                </Form.Group>
                <Form.Group controlId='color' className='my-3'>
                  <Form.Label>Color :</Form.Label>
                  <Form.Control
                    as="select"
                    value={color}
                    onChange={(e) => setColor(e.target.value)}
                  >
                    <option value="">Select Color...</option>
                    {colors.map((loc) => {
                      return (
                        <option key={loc} value={loc}>
                          {loc}
                        </option>
                      );
                    })}
                  </Form.Control>
                </Form.Group>
                <Form.Group controlId='location' className='my-3'>
                  <Form.Label>Location :</Form.Label>
                  <Form.Control
                    as="select"
                    value={location}
                    onChange={(e) => setLocation(e.target.value)}
                  >
                    <option value="">Select Location...</option>
                    {locations.map((loc) => {
                      return (
                        <option key={loc} value={loc}>
                          {loc}
                        </option>
                      );
                    })}
                  </Form.Control>
                </Form.Group>

                <Form.Group>
                    <Form.Label>Set Availability :</Form.Label>
                    <Form.Control
                      as='select'
                      value={isAvailable}
                      onChange={(e) => setIsAvailable(e.target.value)}
                    >
                      <option value="AVAILABLE">Available</option>
                      <option value="UNAVAILABLE">Unavailable</option>
                    </Form.Control>
                  </Form.Group>
                  <Row><Col>
                  <Form.Group controlId="activeDate" className="my-3 form-inline">
                    <Form.Label className="col-xs-2"> Select Start Date :&nbsp;&nbsp; </Form.Label>

                      <DatePicker
                        className="p-2"
                        selected={startDate}
                        onChange={(date) => setStartDate(date.getTime())}
                        minDate={new Date()}
                        dateFormat="dd/MM/yyyy"
                        onKeyDown={(e) => {
                          e.preventDefault();
                        }}
                      />
                  </Form.Group></Col>
                  <Col>
                  <Form.Group controlId="activeDate" className="my-3 form-inline">
                    <Form.Label className="col-xs-2"> Select End date :&nbsp;&nbsp;</Form.Label>
                      <DatePicker
                        className="p-2"
                        selected={endDate}
                        onChange={(date) => setEndDate(date.getTime())}
                        minDate={new Date()}
                        dateFormat="dd/MM/yyyy"
                        onKeyDown={(e) => {
                          e.preventDefault();
                        }}
                      />
                  </Form.Group></Col></Row>
                <Button
                  className='my-3'
                  type="submit"
                  variant='outline-dark'
                >ADD BIKE</Button>
              </Form>
            </CardContent>
          </Col>
        </Row>
      </Card>
    </>
  )
}

export default AddBike
