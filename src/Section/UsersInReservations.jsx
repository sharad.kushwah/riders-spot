import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Row, Col, Table } from "react-bootstrap";
import { useParams } from "react-router-dom";
import Loader from "../Components/Loader"
import ErrorMessage from "../Components/ErrorMessage";
import NoDataFound from "../Components/NoDataFound";
import Paginate from "../Components/Paginate";
import { getAllusersInReservation } from "../Redux/Actions/reservationAction";

const UsersInReservations = () => {
  const [page, setPage] = useState(1);

  const dispatch = useDispatch();
  const params = useParams();

  const reservationUsersList = useSelector(
    (state) => state.reservationUsersList
  );
  const { loading, error, usersReservations, totalCount } =
    reservationUsersList;

    console.log(usersReservations);

  useEffect(() => {
    if (page !== +params.pageNum) {
      setPage(+params.pageNum);
    }
    dispatch(getAllusersInReservation());
  }, [dispatch, params.pageNum]);
  return (
    <>
      {loading && <Loader />}
      {error && <ErrorMessage variant="danger">{error}</ErrorMessage>}
      {error || (usersReservations && usersReservations.length === 0) ? (
        <NoDataFound displayText="No Reservations Found" />
      ) : (
        <Row>
          <Col md={6}>
            <h1>All Users In Reservations : </h1>
          </Col>
          <Table striped bordered hover responsive className="table-sm">
            <thead>
              <tr>
                <th>ID</th>
                <th>Email</th>
                <th>Role</th>
                <th>Bike Id</th>
                <th>Status</th>
                <th>Start Date</th>
                <th>End Date</th>
              </tr>
            </thead>
            <tbody>
              {usersReservations &&
                usersReservations.map((item) => (
                  <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>{item.userEmail}</td>
                    <td>{item.role}</td>
                    <td>{item.bikeId}</td>
                    <td>{item.status}</td>
                    <td>{item.startDate}</td>
                    <td>{item.endDate}</td>
                  </tr>
                ))}
            </tbody>
          </Table>
          <Paginate
            prefix="manage/reservations/users"
            pages={Math.ceil(+totalCount / 5)}
            page={page}
          />
        </Row>
      )}
    </>
  );
};

export default UsersInReservations;
