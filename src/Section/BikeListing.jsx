import React, { useEffect, useState, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams, Link } from "react-router-dom";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { bikeList } from "../Redux/Actions/bikeAction";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import ErrorMessage from "../Components/ErrorMessage";
import NoDataFound from "./NoDataFound";
import BikeItem from "../Components/BikeItem";
import Loader from "../Components/Loader";
import Paginate from '../Components/Paginate'
import debounce from "lodash.debounce";
import { ToastContainer, toast } from "react-toastify";
import axios from "axios"
import * as bikeTypes from "../Redux/Constants/bikeConstants";
import AddBike from "./AddBike";


const BikeListing = () => {
  const [page, setPage] = useState(1);
  const [model, setModel] = useState("");
  const [location, setLocation] = useState("");
  const [avgRating, setAvgRating] = useState("");
  const [color, setColor] = useState("");
  const [showFilter, setShowFilter] = useState(false);
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [createBike, setCreateBike] = useState(false);

  const locations = [
    "Noida",
    "Bengaluru",
    "Chennai",
    "Guwahati",
    "Delhi",
    "Mumbai",
    "Gwalior",
    "Gujarat",
    "Kerala",
    "Jammu"
  ];
  const ratings = [1, 2, 3, 4, 5];
  const bikePerPage = 5;

  const params = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const bikeListing = useSelector((state) => state.bikeList);
  let { loading, bikes, pages, error } = bikeListing;
  const { loggedInUser } = useSelector((state) => state.userLogin)

  const bikeUpdateFunc = useSelector(state => state.bikeUpdate)
  const { loading: loadingUpdateBike, error: errorUpdateBike } = bikeUpdateFunc

  const filterHandler = async () => {
    if (startDate === "" || endDate === "") {
      toast("Dates Cannot be Empty")
      return
    }
    if (startDate > endDate) {
      toast("Start Date cannot be greater than End Date")
      return
    }
    try {
      const res = await axios.get(`https://ridersspot.herokuapp.com/bikes?startDate_gte=${startDate}&endDate_lte=${endDate}&_page=1&_limit=10`)
      console.log(res);
      dispatch({
        type: bikeTypes.bikeListSuccess,
        payload: {
          data: res.data,
          pages: res.headers["x-total-count"]
        }
      })
    }
    catch (error) {
      dispatch({
        type: bikeTypes.bikeListFail,
        payload: error.response && error.response.data ? error.response.data : error.message
      })
      setTimeout(() => {
        dispatch({
          type: bikeTypes.bikeListFailReset,
        })
      }, 3000)
    }
  }

  const ResetFilter = () => {
    setPage(1)
    setModel("")
    setLocation("")
    setAvgRating("")
    setColor("")
    setShowFilter(false)
    setStartDate("")
    setEndDate("")
    dispatch(
      bikeList({
        _page: +params.pageNum || 1,
        _limit: 5,
        model_like: model,
        color,
        location,
        avgRating_gte: avgRating
      }))
  }

  const HandleFilter = () => { setShowFilter(prev => !prev) }

  const handleModel = (e) => {
    setModel(e.target.value);
  }

  const debouncedResults = useMemo(() => {
    return debounce(handleModel, 900);
  }, []);

  useEffect(() => {
    if (page !== +params.pageNum) {
      setPage(+params.pageNum);
    }

    dispatch(
      bikeList({
        _page: +params.pageNum || 1,
        _limit: 5,
        model_like: model,
        color,
        location,
        avgRating_gte: avgRating
      })
    );
  }, [dispatch, params, model, color, location, avgRating, page]);

  return (
    <>
      {
        loading ? (
          <Loader />
        ) : error ? (
          <ErrorMessage variant="danger">{error}</ErrorMessage>
        ) : (
          <>
            {loadingUpdateBike && <Loader />}
            {errorUpdateBike && <ErrorMessage variant="danger">{errorUpdateBike}</ErrorMessage>}
            <Container fluid>
              <Row>
                <Button type="button" variant="dark" style={{ opacity: "0.7", fontSize: 20, width: "100%", padding: "10px 0", margin: "5px 0", marginTop: "2px" }} className="shadow-none" onClick={HandleFilter}>Filter Items</Button>
              </Row>
              {showFilter && <Row>
                <Container style={{ backgroundColor: "#d6cab0" }} className="my-1 font-weight-bold" fluid>
                  <Row>
                    <Col>
                      <Form.Group controlId="color" className="my-3">
                        <Form.Control
                          as="select"
                          value={color}
                          onChange={(e) => setColor(e.target.value)}
                        >
                          <option value="">Select Model Color...</option>
                          <option value="Red">Red</option>
                          <option value="Black">Black</option>
                          <option value="Blue">Blue</option>
                          <option value="Purple">Purple</option>
                          <option value="White">White</option>
                          <option value="Yellow">Yellow</option>
                          <option value="Green">Green</option>
                          <option value="Orange">Orange</option>
                        </Form.Control>
                      </Form.Group>
                    </Col>
                    <Col>
                      <Form.Group controlId="location" className="my-3">
                        <Form.Control
                          as="select"
                          value={location}
                          onChange={(e) => setLocation(e.target.value)}
                        >
                          <option value="">Select Location...</option>
                          {locations.map((loc) => {
                            return (
                              <option key={loc} value={loc}>
                                {loc}
                              </option>
                            );
                          })}
                        </Form.Control>
                      </Form.Group></Col>
                    <Col>
                      <Form.Group controlId="color" className="my-3">
                        <Form.Control
                          as="select"
                          value={avgRating}
                          onChange={(e) => setAvgRating(e.target.value)}
                        >
                          <option value="">Select Rating...</option>
                          {ratings.map((rate) => {
                            return (
                              <option key={rate} value={rate}>{`> ${rate}`}</option>
                            );
                          })}
                        </Form.Control>
                      </Form.Group>
                    </Col>
                    <Col>
                      <Form.Group controlId="model" className="my-3">
                        <ToastContainer></ToastContainer>
                        <Form.Control
                          type="text"
                          placeholder="Enter Model"
                          onChange={debouncedResults}
                        ></Form.Control>
                      </Form.Group>
                    </Col>

                    <Col>
                      <Form.Group controlId="activeDate" className="my-3 form-inline">
                        <Form.Label className="col-xs-2"> From :&nbsp;&nbsp; </Form.Label>
                        <div className="col-xs-10">
                          <DatePicker
                            className="p-2"
                            selected={startDate}
                            onChange={(date) => setStartDate(date.getTime())}
                            minDate={new Date()}
                            dateFormat="dd/MM/yyyy"
                            onKeyDown={(e) => {
                              e.preventDefault();
                            }}
                          /></div>
                      </Form.Group>
                    </Col>

                    <Col>
                      <Form.Group controlId="activeDate" className="my-3 form-inline">
                        <Form.Label className="col-xs-2"> To :&nbsp;&nbsp;</Form.Label>
                        <div className="col-xs-10">
                          <DatePicker
                            className="p-2"
                            selected={endDate}
                            onChange={(date) => setEndDate(date.getTime())}
                            minDate={new Date()}
                            dateFormat="dd/MM/yyyy"
                            onKeyDown={(e) => {
                              e.preventDefault();
                            }}
                          /></div>
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row className="text-center">
                    <Form.Group controlId="activeDateForReservation" >
                      <Button onClick={filterHandler} className="mb-4" size="lg" outline-variant="primary"  >Apply Filter</Button>
                      <Button onClick={ResetFilter} className="mb-4" size="lg" outline-variant="primary"  >Reset Filter</Button>
                    </Form.Group>
                  </Row>
                </Container>
              </Row>}
            </Container>
            {loggedInUser &&
              loggedInUser.user.role === "MANAGER" && !loading && (!page || page<2)? <div className="d-flex justify-content-center w-100 m-0 p-0 ">
              {!createBike ? <Button variant="dark" onClick={() => { setCreateBike(true) }}>Add Bike</Button> :
                <Button
                  className='my-3'
                  onClick={() => { setCreateBike(false) }}
                  variant='danger'
                >Cancel</Button>}
            </div> : null}

            {bikes.length === 0 ? (
              <NoDataFound displayText="No bikes available" />
            ) : (
              <Container>
                {createBike && (!page || page<2) ? <AddBike />:null}
                {bikes.map((bike) => {
                  return (
                    <Col key={bike.id} >
                      <BikeItem bike={bike} />
                    </Col>
                  );
                })}
              </Container>
            )}

            <div className="d-flex justify-content-center">
              <Paginate
                prefix="bikes"
                pages={Math.ceil(+pages / bikePerPage)}
                page={page}
              />
            </div>
          </>
        )}
    </>
  );
};

export default BikeListing
