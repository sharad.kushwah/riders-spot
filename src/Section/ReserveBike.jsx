import React, { useState, useEffect } from "react";
import { useParams, Link, Navigate, useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import {
  Row,
  Col,
  Image,
  ListGroup,
  Card,
  Button,
  Badge,
  Form,
} from "react-bootstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import { BsFillArrowLeftCircleFill } from "react-icons/bs";
import Loader from "../Components/Loader"
import ErrorMessage from "../Components/ErrorMessage";
import Rating from "../Components/Rating";
import { createReservation } from "../Redux/Actions/reservationAction"
import { createReview } from "../Redux/Actions/reviewAction";
import { listReviews } from "../Redux/Actions/reviewAction";
import { getBikeDetails, updateBike } from "../Redux/Actions/bikeAction";

const ReserveBike = () => {
  const params = useParams();
  const dispatch = useDispatch();

  const [startDate, setStartDate] = useState((new Date()));
  const [endDate, setEndDate] = useState((new Date()));
  const [rating, setRating] = useState(0);
  const [comment, setComment] = useState("");
  const { loading, error, bike } = useSelector((state) => state.bikeDetails);
  const reviewListing = useSelector((state) => state.reviewList);

  const {
    loading: loadingReviews,
    error: errorReview,
    reviews
  } = reviewListing;

  const reviewCreateFunc = useSelector((state) => state.reviewCreate);
  const { loading: createReviewLoading, error: createReviewError } =
    reviewCreateFunc;

  const userLogin = useSelector((state) => state.userLogin);
  const { loading: userLoading, error: userError, loggedInUser } = userLogin;

  const createReservations = useSelector((state) => state.reservationCreator);
  const {
    loading: reserveLoading,
    error: reserveError,
    success,
  } = createReservations;


  const formatDate = (date) => {
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();
    return `${year}/${month}/${day}`;
  };

  const submitHandler = (e) => {
    e.preventDefault();
    if (rating === 0) {
      toast("Please select a rating");
      return;
    }

    if (comment.length === 0) {
      toast("Comment cannot be empty");
      return;
    }
    dispatch(
      createReview({
        rating,
        comment,
        bikeId: +params.bikeId,
        userId: loggedInUser.user.id,
      })
    );
    dispatch(listReviews(+params.bikeId));
    setRating(0);
    setComment("");
  };

  useEffect(() => {
    dispatch(getBikeDetails(+params.bikeId));
    dispatch(listReviews(+params.bikeId));
  }, [dispatch, params.bikeId]);

  const bookHandler = (e) => {
    if (bike.startDate > startDate.getTime() || startDate.getTime() > bike.endDate || endDate.getTime() < bike.startDate || endDate.getTime() > bike.endDate) {
      toast("Enter dates in availability of bike")
      return;
    }
    
    if(startDate.getTime() < endDate.getTime()) {
      dispatch(
        createReservation({
          bikeId: +params.bikeId,
          userId: loggedInUser.user.id,
          start_date: formatDate(startDate),
          end_date: formatDate(endDate),
          status: "BOOKED",
        })
      );
      dispatch(
        updateBike({
          id: +params.bikeId,
          isAvailable: false
        })
      );
      return;
    }

    if (startDate.getTime() === endDate.getTime()) {
      toast("Start Date cannot be equal to End date");
      return;
    }
    else {
      toast("Start Date cannot be greater than End date");
      return;
    }
  };
  const navigate = useNavigate();
  return (
    <>
      <Button
        variant='light'
        onClick={() => { navigate(-1); }}
        className="position-absolute m-3"
      >
        Go Back </Button>

      {loading ? (
        <Loader />
      ) : error ? (
        <ErrorMessage variant="danger">{error}</ErrorMessage>
      ) : (
        <>

          <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-around  ", padding: "5vh" }}>
            <div>
              <Image
                src={`/images/${bike.image}`}
                alt={`Bike image: ${bike.model}`}
                style={{height:"500px"}}
                fluid
              />

              {reserveLoading && <Loader />}
              {reserveError && (
                <ErrorMessage variant="danger">{reserveError}</ErrorMessage>
              )}
              {success && (
                <ErrorMessage variant="success">{"Reservation Created Successfully"}</ErrorMessage>
              )}

              <ListGroup variant="flush">
                <ListGroup.Item>
                  <h5>{bike.model}</h5>
                </ListGroup.Item>
                <ListGroup.Item>
                  <Rating value={bike.avgRating} />
                </ListGroup.Item>
                <ListGroup.Item>
                  <h5>{bike.color}</h5>
                </ListGroup.Item>
                <ListGroup.Item>
                  <h5>{bike.location}</h5>
                </ListGroup.Item>
              </ListGroup>
            </div>
            <div md={4} >
              <Card>
                <ListGroup variant="flush" style={{ width: "100%" }}>
                  <ListGroup.Item>
                    <Row>
                      <Col>Status</Col>
                      <Col>
                        {bike.isAvailable ? (
                          <Badge pill bg="success" text="light">
                            Available
                          </Badge>
                        ) : (
                          <Badge pill bg="danger" text="light">
                            Unavailable
                          </Badge>
                        )}
                      </Col>
                    </Row>
                  </ListGroup.Item>
                  <ListGroup.Item>
                    <Row>
                      <Col>Start Date</Col>
                      <Col>
                        <DatePicker
                          selected={startDate}
                          onChange={(date) => setStartDate(date)}
                          minDate={new Date()}
                          dateFormat="yyyy/MM/dd"
                          onKeyDown={(e) => {
                            e.preventDefault();
                          }}
                        />
                      </Col>
                    </Row>
                  </ListGroup.Item>
                  <ListGroup.Item>
                    <Row>
                      <Col>End Date</Col>
                      <Col>
                        <DatePicker
                          selected={endDate}
                          onChange={(date) => setEndDate(date)}
                          minDate={new Date()}
                          dateFormat="yyyy/MM/dd"
                          onKeyDown={(e) => {
                            e.preventDefault();
                          }}
                        />
                      </Col>
                    </Row>
                  </ListGroup.Item>
                  <ListGroup.Item>
                    <ToastContainer></ToastContainer>
                    <Button
                      type="button"
                      className="btn-block"
                      onClick={bookHandler}
                      disabled={!bike.isAvailable}
                    >
                      Book Now
                    </Button>
                  </ListGroup.Item>
                </ListGroup>
              </Card>

              <ListGroup variant="flush" className="mt-5">
                <ListGroup.Item>
                  <h4>Write a review</h4>
                  {createReviewLoading && <Loader />}
                  {createReviewError && (
                    <ErrorMessage variant="danger">{createReviewError}</ErrorMessage>
                  )}
                  <Form onSubmit={submitHandler}>
                    <Form.Group controlId="rating">
                      <Form.Label>Rating</Form.Label>
                      <Form.Control
                        as="select"
                        value={rating}
                        onChange={(e) => setRating(e.target.value)}
                      >
                        <option value="0">Select...</option>
                        <option value="1">1 - Poor</option>
                        <option value="2">2 - Fair</option>
                        <option value="3">3 - Average</option>
                        <option value="4">4 - Good</option>
                        <option value="5">5 - Excellent</option>
                      </Form.Control>
                    </Form.Group>

                    <Form.Group controlId="comment">
                      <Form.Label>Comment</Form.Label>
                      <Form.Control
                        as="textarea"
                        row="3"
                        value={comment}
                        onChange={(e) => setComment(e.target.value)}
                      ></Form.Control>
                    </Form.Group>
                    <Button className="my-3" type="submit" variant="primary">
                      Submit
                    </Button>
                  </Form>
                </ListGroup.Item>
                <h4 className="m-3">Reviews</h4>
                {reviews && reviews.length === 0 && (
                  <ErrorMessage>No Reviews</ErrorMessage>
                )}
                {reviews &&
                  reviews.map((review) => (
                    <ListGroup.Item key={review.id}>
                      <Rating value={review.rating} />
                      <p>{review.comment}</p> 
                    </ListGroup.Item>
                  ))}
              </ListGroup>
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default ReserveBike;
