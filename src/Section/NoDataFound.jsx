import React from 'react';
import { useNavigate, Link } from "react-router-dom";
import { Image, Button, Container } from "react-bootstrap";
const NoDataFound = () => {
    const navigate = useNavigate();

    const handleClick = () => {
        navigate("/bikes");
    }

    return (

        <div>

               <Container>
                 <Button
                     variant='light'
                     onClick={handleClick}
                 >
                     Go Back </Button>
               </Container>
            <Container md={6} className="d-flex flex-column align-items-center">
                <Link to="/bikes">
                    <Image
                        src="https://img.freepik.com/free-vector/no-data-concept-illustration_114360-616.jpg?t=st=1655268979~exp=1655269579~hmac=5e76b31397748b54696df53b86c839a70b5abbba2a19df022fb81ded98ad9054&w=826"
                        alt="No Data Found"
                        fluid
                    ></Image>
                </Link>
            </Container>

        </div>

    )
}

export default NoDataFound