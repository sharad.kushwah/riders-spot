import React, { useState, useEffect } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux';
import { login } from '../Redux/Actions/userAction';
import ErrorMessage from '../Components/ErrorMessage';
import Loader from '../Components/Loader';
import { Button } from 'react-bootstrap';

const Login = () => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [message, setMessage] = useState("");
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const userLogin = useSelector(state => state.userLogin);
    const { loading, error, loggedInUser } = userLogin;

    
    const emailValidation = () => {
        if( /(.+)@(.+){2,}\.(.+){2,}/.test(email) ){
            // valid email
            return true;
          } else {
              // invalid email
              setMessage("Invalid email!")
              return false;
          }
        }
    
    const handleClick = (e) => {
        e.preventDefault();
        if(emailValidation()){
            if(password.length>=6){
                setMessage(null)
                dispatch(login(email, password));
            }else 
            {
                setMessage("Password is too short!");
            }
        }
    }

    useEffect(() => {
        if (loggedInUser && loggedInUser.user) {
            navigate("/bikes", { replace: true });
        }
        else{
            console.log(loggedInUser);
        }
    }, [loggedInUser, navigate])

    return (
        <section className='Form my-5 py-5'>

            <div className="container">
                <div className="row g-0 align-items-center">
                    <div className="col-lg-5 ">
                        <img src='/Images/AuthSection.jpg' alt="" className='img-fluid' />
                    </div>
                    <div className="col-lg-7 px-4 pl-5">
                        <h1 className='d-flex justify-content-center'>Riders Spot</h1>
                        <h4 className='d-flex justify-content-center'>Sign into your account</h4>
                        <form action="">
                            {(loading || error || message) && 
                            <div className='form-row d-flex justify-content-center'>
                                    {loading && <Loader />}
                                    {error && <ErrorMessage variant='danger'>{error}</ErrorMessage>}
                                    {message && <ErrorMessage variant='danger'>{message}</ErrorMessage>}
                                    </div>                                
                            }
                            <div className="form-row d-flex justify-content-center">
                                <div className="col-lg-7">
                                    <input type="email" name="email" onChange={(e) => setEmail(e.target.value)} placeholder='Email-address' className='form-control my-3 p-4' />
                                </div>
                            </div>
                            <div className="form-row d-flex justify-content-center">
                                <div className="col-lg-7">
                                    <input type="password" name="password" onChange={(e) => setPassword(e.target.value)} placeholder='******' className="form-control my-3 p-4" />
                                </div>
                            </div>
                            <div className="form-row d-flex justify-content-center">
                                <div className="col-lg-7 d-flex justify-content-center">
                                    <Button variant="dark" type="submit" className="btn1 mt-3 mb-5 " onClick={handleClick}>Login</Button>
                                </div>
                            </div>
                            <p className='d-flex justify-content-center'>Don't have an account? <Link to='/signup'>Sign up</Link></p>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Login