import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Row, Col, Table, Container } from "react-bootstrap";
import { useParams } from "react-router-dom";
import Loader from "../Components/Loader";
import ErrorMessage from "../Components/ErrorMessage";
import NoDataFound from "./NoDataFound";
import Paginate from "../Components/Paginate";
import { getAllBikesInReservation } from "../Redux/Actions/reservationAction";

const BikeReserveList = () => {
  const [page, setPage] = useState(1);
  const dispatch = useDispatch();
  const params = useParams();

  const reservationBikesList = useSelector(
    (state) => state.reservationBikesList
  );

  let { loading, error, bikesReservations, totalCount } = reservationBikesList;
  console.log(params.pageNum);
  useEffect(() => {
        if (page !== +params.pageNum) {
          setPage(+params.pageNum);
        }
        dispatch(getAllBikesInReservation(params.pageNum, 5, params.bikeId))
  }, [dispatch, params.pageNum]);

  return (
    <Container style={{marginTop: "100px"}}>
      {" "}
      {loading && <Loader />}
      {error && <ErrorMessage variant="danger">{error}</ErrorMessage>}
      {error || (bikesReservations && bikesReservations.length === 0) ? (
        <NoDataFound displayText="No Reservations Found" />
      ) : (
        <Row>
          <Table className="table-sm">
            <thead>
              <tr>
                <th>ID</th>
                <th>Model</th>
                <th>Color</th>
                <th>Location</th>
                <th>Avg Rating</th>
                <th>Start Date</th>
                <th>End Date</th>
              </tr>
            </thead>
            <tbody>
              {bikesReservations &&
                bikesReservations.map((item) => (
                  <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>{item.model}</td>
                    <td>{item.color}</td>
                    <td>{item.location}</td>
                    <td>{item.avgRating}</td>
                    <td>{item.startDate}</td>
                    <td>{item.endDate}</td>
                  </tr>
                ))}
            </tbody>
          </Table>
          <div className="d-flex justify-content-center">
          <Paginate
            prefix={`reservations/${params.bikeId}`}
            pages={Math.ceil(+totalCount / 5)}
            page={page}
          /></div>
        </Row>
      )}
    </Container>
  );
};

export default BikeReserveList;
