import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom'
import { Button } from 'react-bootstrap';
import ErrorMessage from '../Components/ErrorMessage';
import Loader from '../Components/Loader';
import { register } from '../Redux/Actions/userAction';

const SignUp = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [message, setMessage] = useState(null);

    const navigate = useNavigate();

    const userRegister = useSelector(state => state.userRegister);
    const { error, loading, loggedInUser } = userRegister;
    const dispatch = useDispatch();

     const emailValidation = () => {
        if( /(.+)@(.+){2,}\.(.+){2,}/.test(email) ){
            // valid email
            return true;
          } else {
              // invalid email
              setMessage("Invalid email!")
              return false;
          }
        }

    const handleClick = (e) => {
        e.preventDefault()
        if (emailValidation()) {
            if (password.length >= 6) {
                if (password !== confirmPassword) {
                    setMessage("Passwords do not match")
                } else {
                    setMessage(null)
                    dispatch(register(email, password));
                }
            } else {
                setMessage("Password is too short!");
            }

        }
    }

    useEffect(() => {
        if (loggedInUser) {
            navigate("/login", { replace: true })
            window.location.reload(false);
        }
    }, [loggedInUser, navigate])

    return (
        <section className='Form my-5 py-5'>

            <div className="container">
                <div className="row g-0 align-items-center">
                    <div className="col-lg-5 ">
                        <img src='/Images/AuthSection.jpg' alt="" className='img-fluid' />
                    </div>
                    <div className="col-lg-7 px-4 pl-5">
                        <h1 className='d-flex justify-content-center'>Riders Spot</h1>
                        <h4 className='d-flex justify-content-center'>Create an account</h4>
                        <form action="">
                            {(loading || error || message) &&
                                <div className='form-row d-flex justify-content-center'>
                                    {loading && <Loader />}
                                    {message && <ErrorMessage variant='danger'>{message}</ErrorMessage>}
                                    {error && <ErrorMessage variant='danger'>{error}</ErrorMessage>}
                                </div>
                            }
                            <div className="form-row d-flex justify-content-center">
                                <div className="col-lg-7">
                                    <input type="email" onChange={(e) => setEmail(e.target.value)} name="email" placeholder='Email-address' className='form-control my-3 p-4' />
                                </div>
                            </div>
                            <div className="form-row d-flex justify-content-center">
                                <div className="col-lg-7">
                                    <input type="password" name="password" onChange={(e) => setPassword(e.target.value)} placeholder='Password' className="form-control my-3 p-4" />
                                </div>
                            </div>
                            <div className="form-row d-flex justify-content-center">
                                <div className="col-lg-7">
                                    <input type="password" name="password" onChange={(e) => setConfirmPassword(e.target.value)} placeholder='Confirm Password' className="form-control my-3 p-4" />
                                </div>
                            </div>
                            <div className="form-row d-flex justify-content-center">
                                <div className="col-lg-7 d-flex justify-content-center">
                                    <Button variant="dark" className="btn1 mt-3 mb-5" onClick={handleClick}>Sign up</Button>
                                </div>
                            </div>
                            <p className='d-flex justify-content-center'>Already have an account?&nbsp;<Link to='/login'>&nbsp;Login</Link></p>

                        </form>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default SignUp