import React, { useState, useEffect } from 'react';
import { useNavigate, useParams } from "react-router-dom";
import { Row, Col, Button, Container } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import Loader from "../Components/Loader"
import ErrorMessage from "../Components/ErrorMessage";
import NoDataFound from "../Components/NoDataFound";
import Paginate from "../Components/Paginate";
import { getAllUsers, deleteUser } from '../Redux/Actions/userAction';
import UserCard from "../Components/UserCard"

const ManageUsers = () => {
  const [page, setPage] = useState(1);
  const dispatch = useDispatch()
  const params = useParams()
  const navigate = useNavigate()

  const usersList = useSelector(state => state.usersList)
  const {loggedInUser} = useSelector(state => state.userLogin)
  const { loading, error, users, totalCount } = usersList

  useEffect(() => {
    if (page !== +params.pageNum) {
      setPage(+params.pageNum)
    }
    dispatch(getAllUsers(+params.pageNum, 5))
  }, [dispatch, page, params.pageNum])

  const createUser=()=>{
    navigate('/create/users')
  }

  return (
    <>
      {loading && <Loader />}
      {error && <ErrorMessage variant='danger'>{error}</ErrorMessage>}
      {users && users.length === 0 ? (
        <NoDataFound displayText="No user found" />
      ) : (
        <>
        {loggedInUser && !loading &&
                loggedInUser.user.role==="MANAGER"? <div className="d-flex justify-content-center w-100 m-0 p-0 ">
                    <Button variant="dark" onClick={()=>createUser()}>Add User</Button>
                </div> : null}
        <div className='d-flex flex-wrap justify-content-center'>
              {users && users.map((user) => <UserCard user={user} />)}
            </div>
          <div className='d-flex justify-content-center'><Paginate
            page={page}
            pages={Math.ceil((+totalCount) / 5)}
            prefix="manage/users"
          /></div></>
      )}
    </>
  )
}

export default ManageUsers
