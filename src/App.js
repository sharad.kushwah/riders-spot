import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from "./Section/Login";
import SignUp from "./Section/SignUp";
import Footer from "./Components/Footer";
import Header from "./Components/Header";
import BikeListing from "./Section/BikeListing";
import Authentication from "./Components/Authentication";
import ReserveBike from "./Section/ReserveBike";
import UserReservations from "./Section/UserReservations";
import ManageUsers from "./Section/ManageUsers";
import UsersInReservations from "./Section/UsersInReservations";
import BikesInReservation from "./Section/BikesInReservation";
import AddBike from "./Section/AddBike";
import AddUser from "./Section/AddUser";
import NoDataFound from "./Section/NoDataFound";
import BikeReserveList from "./Section/BikeReserveList";
import ManagerReservations from "./Section/ManagerReservations";
import './index.css'

function App() {
  return (
    <BrowserRouter>
      <div
        style={{
          backgroundColor: "Vanilla",
          height: "100vh",
          display: "flex",
          justifyContent: "space-between",
          flexDirection: "column",
        }}
      >
        <div className="header-app">
          <Header />
          
          <Routes>
              <Route path="/" element={<Login />} />
              <Route path="/login" element={<Login />} />
              <Route path="/signup" element={<SignUp />} />
              <Route
                path="/bikes"
                element={
                  <Authentication >
                    <BikeListing />
                  </Authentication>
                }
              />
              <Route
                path="/bikes/page/:pageNum"
                element={
                  <Authentication >
                    <BikeListing  />
                  </Authentication>
                }
              />
              <Route
                path="/bikes/:bikeId"
                element={
                  <Authentication>
                    <ReserveBike />
                  </Authentication>
                }
              />
              <Route
                path="/reservations/:bikeId"
                element={
                  <Authentication accessRole="MANAGER">
                    <BikeReserveList />
                  </Authentication>
                }
              />
              <Route
                path="/reservations/:bikeId/page/:pageNum"
                element={
                  <Authentication accessRole="MANAGER">
                    <BikeReserveList />
                  </Authentication>
                }
              />
              
              <Route
                path="users/:userId/reservations"
                element={
                  <Authentication>
                    <UserReservations />
                  </Authentication>
                }
              />
              <Route
                path="/manage/users"
                element={
                  <Authentication accessRole="MANAGER">
                    <ManageUsers />
                  </Authentication>
                }
              />
              <Route
                path="/manage/:userId/myreservations"
                element={
                  <Authentication accessRole="MANAGER">
                    <ManagerReservations />
                  </Authentication>
                }
              />
              <Route
                path="/manage/users/page/:pageNum"
                element={
                  <Authentication accessRole="MANAGER">
                    <ManageUsers />
                  </Authentication>
                }
              />
              <Route
                path="/manage/reservations/users"
                element={
                  <Authentication accessRole="MANAGER">
                    <UsersInReservations />
                  </Authentication>
                }
              />
              <Route
                path="/manage/reservations/users/page/:pageNum"
                element={
                  <Authentication accessRole="MANAGER">
                    <UsersInReservations />
                  </Authentication>
                }
              />
              <Route
                path="/manage/reservations/bikes"
                element={
                  <Authentication accessRole="MANAGER">
                    <BikesInReservation />
                  </Authentication>
                }
              />
              <Route
                path="/manage/reservations/bikes/page/:pageNum"
                element={
                  <Authentication accessRole="MANAGER">
                    <BikesInReservation />
                  </Authentication>
                }
              />
              <Route
                path="/create/users"
                element={
                  <Authentication accessRole="MANAGER">
                    <AddUser />
                  </Authentication>
                }
              />
              <Route
                path="/create/bikes"
                element={
                  <Authentication accessRole="MANAGER">
                    <AddBike />
                  </Authentication>
                }
              />
              <Route path="*" element={<NoDataFound />} />
            </Routes>
        </div>
        <Footer />
      </div>
    </BrowserRouter>
    
  );
}

export default App;
